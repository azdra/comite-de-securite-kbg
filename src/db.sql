START TRANSACTION;

-- ============================================================
--   Suppression et création de la base de données 
-- ============================================================
DROP DATABASE IF EXISTS kgb;
CREATE DATABASE kgb;
USE kgb;

CREATE TABLE Administrator
(
    admin_id         INT AUTO_INCREMENT NOT NULL,
    admin_pseudo     VARCHAR(255)       NULL,

    admin_last_name  VARCHAR(45)        NULL,
    admin_first_name VARCHAR(45)        NULL,
    admin_email      VARCHAR(45)        NULL,
    admin_password   VARCHAR(60)        NULL,
    admin_creation   DATE DEFAULT NULL,
    CONSTRAINT PK_admin PRIMARY KEY (admin_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE status_type
(
    status_id   INT AUTO_INCREMENT NOT NULL,

    status_name VARCHAR(45)        NULL,
    CONSTRAINT PK_status PRIMARY KEY (status_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE mission_type
(
    mission_type_id   INT AUTO_INCREMENT NOT NULL,

    mission_type_name VARCHAR(45)        NULL,
    CONSTRAINT PK_mission_type PRIMARY KEY (mission_type_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE hideout_type
(
    hideout_type_id   INT AUTO_INCREMENT NOT NULL,

    hideout_type_name VARCHAR(45)        NULL,
    CONSTRAINT PK_hideout_type PRIMARY KEY (hideout_type_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE speciality_type
(
    speciality_id   INT AUTO_INCREMENT NOT NULL,

    speciality_name VARCHAR(45)        NULL,
    CONSTRAINT PK_speciality PRIMARY KEY (speciality_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE country
(
    country_id          INT AUTO_INCREMENT NOT NULL,

    country_name        VARCHAR(45)        NULL,
    country_nationality VARCHAR(45)        NULL,
    CONSTRAINT PK_country PRIMARY KEY (country_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE contact
(
    contact_id          INT AUTO_INCREMENT NOT NULL,
    contact_code        VARCHAR(45),

    contact_last_name   VARCHAR(45)        NULL,
    contact_first_name  VARCHAR(45)        NULL,
    contact_birth       DATE,
    contact_nationality INT                NOT NULL,

    CONSTRAINT PK_contact PRIMARY KEY (contact_id, contact_code),
    CONSTRAINT FK_contact_country FOREIGN KEY (contact_nationality) REFERENCES country (country_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE agent
(
    agent_id          INT AUTO_INCREMENT NOT NULL,
    agent_code        VARCHAR(45),

    agent_last_name   VARCHAR(45)        NULL,
    agent_first_name  VARCHAR(45)        NULL,
    agent_birth       DATE DEFAULT NULL,
    agent_nationality INT                NOT NULL,
    agent_speciality  INT,

    CONSTRAINT PK_agent PRIMARY KEY (agent_id, agent_code),
    CONSTRAINT FK_Agent_country FOREIGN KEY (agent_nationality) REFERENCES country (country_id),
    CONSTRAINT FK_Agent_speciality FOREIGN KEY (agent_speciality) REFERENCES speciality_type (speciality_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE target
(
    target_id          INT AUTO_INCREMENT NOT NULL,
    target_code        VARCHAR(45),

    target_last_name   VARCHAR(45)        NULL,
    target_first_name  VARCHAR(45)        NULL,
    target_birth       DATE DEFAULT NULL,
    target_nationality INT                NOT NULL,
    CONSTRAINT PK_target PRIMARY KEY (target_id, target_code),
    CONSTRAINT FK_target_country FOREIGN KEY (target_nationality) REFERENCES country (country_id)
);

CREATE TABLE hideout
(
    hideout_id      INT AUTO_INCREMENT NOT NULL,
    hideout_code    VARCHAR(45),
    hideout_country INT                NOT NULL,
    hideout_address VARCHAR(45),
    hideout_type    INT                NOT NULL,

    CONSTRAINT PK_hideout_id PRIMARY KEY (hideout_id),
    CONSTRAINT FK_hideout_country FOREIGN KEY (hideout_country) REFERENCES country (country_id),
    CONSTRAINT FK_hideout_type FOREIGN KEY (hideout_type) REFERENCES hideout_type (hideout_type_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE mission
(
    mission_code        INT AUTO_INCREMENT NOT NULL,

    mission_title       VARCHAR(45)        NULL,
    mission_description VARCHAR(45)        NULL,
    mission_country     INT                NOT NULL,
    mission_agent       INT                NOT NULL,
    mission_contact     INT                NOT NULL,
    mission_target      INT                NOT NULL,
    mission_type        INT                NOT NULL,
    mission_status      INT                NOT NULL,
    mission_nb_hideout  INT                NOT NULL,
    mission_speciality  INT                NOT NULL,
    mission_start       DATE DEFAULT NULL,
    mission_end         DATE DEFAULT NULL,

    CONSTRAINT PK_mission PRIMARY KEY (mission_code),
    CONSTRAINT FK_Mission_country FOREIGN KEY (mission_country) REFERENCES country (country_id),
    CONSTRAINT FK_Mission_Agent FOREIGN KEY (mission_agent) REFERENCES agent (agent_id),
    CONSTRAINT FK_Mission_target FOREIGN KEY (mission_target) REFERENCES target (target_id),
    CONSTRAINT FK_Mission_Contact FOREIGN KEY (mission_contact) REFERENCES contact (contact_id),
    CONSTRAINT FK_Mission_Type FOREIGN KEY (mission_type) REFERENCES mission_type (mission_type_id),
    CONSTRAINT FK_Mission_Status FOREIGN KEY (mission_status) REFERENCES status_type (status_id),
    CONSTRAINT FK_Mission_speciality FOREIGN KEY (mission_speciality) REFERENCES speciality_type (speciality_id)
);

-- ============================================================
--   Insertion des enregistrements
-- ============================================================

-- ADMINISTRATION
INSERT INTO Administrator
VALUES
-- (id, pseudo, last_name, first_name, email, password, creation)
(1, 'Vlad', 'Vladimir', 'Poutine', 'poutine@kgb.ru', '8aNadTQqN3cNhEgyxZSshcvschwgRdXQdCB37g3LVQa4', '2020-09-18'),
(NULL, 'xX_Duck_Du_38_Xx', 'Donald', 'Trump', 'kanar@coin.eu', '123', '2020-09-18');

-- STATUS
INSERT INTO status_type
VALUES
-- (id, last_name_du_status)
(1, 'En preparation'),
(NULL, 'En cours'),
(NULL, 'Terminé'),
(NULL, 'Echec');

-- MISSION TYPE
INSERT INTO mission_type
VALUES
-- (id, last_name_du_status)
(1, 'Surveillance'),
(NULL, 'Assassinat'),
(NULL, 'Infiltration');

-- speciality TYPE
INSERT INTO speciality_type
VALUES
-- (id, type_de_speciality)
(1, 'Filature'),
(NULL, 'Couverture'),
(NULL, 'Assassinat'),
(NULL, 'Infiltration'), -- 4
(NULL, 'Kidnapping'),
(NULL, 'Patisserie');

INSERT INTO country
VALUES (1, 'France', 'Français'),
       (NULL, 'Etats-Unis', 'Americains'),
       (NULL, 'Ukraine', 'Ukrainiens'),
       (NULL, 'Allemagne', 'Allemands'),
       (NULL, 'Japon', 'Japonais'),
       (NULL, 'Égypte', 'Égyptiens'),
       (NULL, 'Jajaland', 'Jajanais'),
       (NULL, 'Russie', 'Russe'),      -- 8
       (NULL, 'Brésil', 'Brésiliens'), -- 9
       (NULL, 'Australie', 'Australiens'),
       (NULL, 'Angleterre', 'Anglais'),
       (NULL, 'Italie', 'Italiens'),
       (NULL, 'Inde', 'Indiens'),
       (NULL, 'Grèce', 'Grecque'),
       (NULL, 'Suède', 'Suèdois');

-- AGENT 
INSERT INTO agent
VALUES
-- (last_name_de_code, last_name, first_name, date_de_birth, id_du_country_de_la_nationality, id_de_la_speciality)
(1, '0123', 'Dupont', 'François', '1976-03-07', 1, 1),
(NULL, '0996', 'Mohammed', 'Abdul', '1986-01-16', 6, 2),
(NULL, '0421', 'Ruski', 'Vladimir', '1974-08-27', 8, 3),
(NULL, '0690', 'Bottlehead', 'Henri', '1988-11-01', 11, 5),
(NULL, '0444', 'Donkey', 'Bong', '1988-11-01', 9, 4),
(NULL, '0740', 'Spankman', 'Michael', '1980-01-13', 10, 6);

-- target
INSERT INTO target
VALUES
-- (last_name_de_code, last_name, first_name, date_de_birth, id_du_country_de_la_nationality)
(1, 'La morue', 'Moriau', 'Charle', '1968-05-13', 1),
(NULL, 'Le marchand', 'Vespucci', 'Antonio', '1960-12-06', 12),
(NULL, 'La colombe', 'Endrew', 'White', '1975-07-24', 2),
(NULL, "L'enchantresse", 'Anastasia', 'Ivanov', '1980-03-30', 8),
(NULL, 'Le fumeur', 'Rassim', 'Mindou', '1976-09-21', 13),
(NULL, 'La fumeuse', 'Namia', 'Mindou', '1979-03-28', 13);

-- CONTACTS
INSERT INTO contact
VALUES
-- (last_name_de_code, last_name, first_name, date_de_birth, id_du_country_de_la_nationality)
(1, "L'étoile", 'Belcourti', 'Betelgeuse', '1969-12-30', 2),
(NULL, 'La tour', 'Giovanna', 'Luigi', '1973-08-14', 12),
(NULL, "L'abeille", 'Mourra', 'Sophitia', '1980-04-16', 14),
(NULL, 'La bouteille', 'Ivankof', 'Nikolay', '1965-10-08', 8),
(NULL, "L'oreille", 'Sven', 'Berg', '1985-01-30', 15);

-- hideout TYPE
INSERT INTO hideout_type
VALUES
-- (id, last_name_du_type)
(1, 'Villa'),
(NULL, 'Maison'),
(NULL, 'Patisserie');

COMMIT;
