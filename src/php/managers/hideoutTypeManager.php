<?php


include_once dirname(__DIR__) . '/models/hideout_type.php';

class HideoutTypeManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM hideout_type');


        while($row = $stmt->fetch()) {
            $hideoutType = new HideoutType();
            $hideoutType->setId($row['hideout_type_id']);
            $hideoutType->setHideoutTypeName($row['hideout_type_name']);

            $result[] = $hideoutType;
        }

        return $result;
    }    

    public function addHideoutType($hideoutType)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO hideout_type (hideout_type_name) VALUES (:ptName);');
        $stmt->execute([
            'ptName' => $hideoutType->getHideoutTypeName()
        ]);
    }
}
