<?php

require dirname(__DIR__).'/models/target.php';

class targetManager extends DBConnect {

    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM target');

        while($row = $stmt->fetch()) {
            $target = new Target();
            $target->setId($row['target_id']);
            $target->setCode($row['target_code']);
            $target->setLastName($row['target_last_name']);
            $target->setFirstName($row['target_first_name']);
            $target->setBirth($row['target_birth']);
            $target->setNationality($row['target_nationality']);

            $result[] = $target;
        }

        return $result;
    }

    public function addTarget($target)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO target (target_code, target_last_name, target_first_name, target_birth, target_nationality) VALUES (:tCode, :tLast, :tFirst, :tBirth, :tNationality);');
        $stmt->execute([
            "tCode" => $target->getCode(),
            "tLast" => $target->getLastName(),
            "tFirst" => $target->getFirstName(),
            "tBirth" => $target->getBirth(),
            "tNationality" => $target->getNationality(),
        ]);
        return $target;
    }
}