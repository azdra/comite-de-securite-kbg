<?php

include dirname(__DIR__).'/models/agent.php';

class AgentManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM agent');

        while($row = $stmt->fetch()) {
            $agent = new Agent();
            $agent->setId($row['agent_id']);
            $agent->setCode($row['agent_code']);
            $agent->setLastName($row['agent_last_name']);
            $agent->setFirstName($row['agent_first_name']);
            $agent->setBirth($row['agent_birth']);
            $agent->setNationality($row['agent_nationality']);
            $agent->setSpeciality($row['agent_speciality']);

            $result[] = $agent;
        }

        return $result;
    }

    public function addAgent($agent)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO agent (agent_code, agent_last_name, agent_first_name, agent_birth, agent_nationality, agent_speciality) VALUES (:aCode, :aNom, :aPrenom, :aNaissance, :aNationalite, :aSpecialite);');
        $stmt->execute([
            'aCode' => $agent->getCode(),
            'aNom' => $agent->getLastName(),
            'aPrenom' => $agent->getFirstName(),
            'aNaissance' => $agent->getBirth(),
            'aNationalite' => $agent->getNationality(),
            'aSpecialite' => $agent->getSpeciality(),
        ]);
        return true;
    }
}