<?php


require_once dirname(__DIR__).'/models/admin.php';

class AdminManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM Administrator');

        while($row = $stmt->fetch()) {
            $admin = new Admin();
            $admin->setId($row['admin_id']);
            $admin->setPseudo($row['admin_pseudo']);
            $admin->setLastName($row['admin_last_name']);
            $admin->setFirstName($row['admin_first_name']);
            $admin->setMail($row['admin_email']);
            $admin->setPassword($row['admin_password']);
            $admin->setCreation($row['admin_creation']);

            $result[] = $admin;
        }

        return $result;
    }

    public function addAdmin($admin)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Administrator (admin_pseudo, admin_last_name, admin_first_name, admin_email, admin_password, admin_creation) VALUES (:pseudo, :lastName, :firstName, :email, :pass, :creat);');
        $stmt->execute([
            'pseudo' => $admin->getPseudo(),
            'lastName' => $admin->getLastName(),
            'firstName' => $admin->getFirstName(),
            'email' => $admin->getMail(),
            'pass' => $admin->getPassword(),
            'creat' => $admin->getCreation(),
        ]);
        return true;
    }
}
