<?php

include_once dirname(__DIR__).'/models/hideout.php';

class HideoutManager extends DBConnect {

    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM hideout');

        while($row = $stmt->fetch()) {
            $country = new Hideout();
            $country->setId($row['hideout_id']);
            $country->setCode($row['hideout_code']);
            $country->setCountry($row['hideout_country']);
            $country->setAddress($row['hideout_address']);
            $country->setType($row['hideout_type']);

            $result[] = $country;
        }

        return $result;
    }


    public function addHideout($hideout)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO hideout (hideout_code, hideout_country, hideout_address, hideout_type) VALUES (:hCode, :hCountry, :hAddress, :hType)');
        $stmt->execute([
            'hCode' => $hideout->getCode(),
            'hCountry' => $hideout->getCountry(),
            'hAddress' => $hideout->getAddress(),
            'hType' => $hideout->getType(),
        ]);
        return $hideout;
    }
}