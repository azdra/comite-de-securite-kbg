<?php


include_once dirname(__DIR__).'/models/speciality.php';

class SpecialityManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM speciality_type');


        while($row = $stmt->fetch()) {
            $speciality = new Speciality();
            $speciality->setId($row['speciality_id']);
            $speciality->setSpecialityName($row['speciality_name']);


            $result[] = $speciality;
        }

        return $result;
    }

    public function addSpeciality($speciality)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO speciality_type (speciality_name) VALUES (:spName)');
        $stmt->execute([
            'spName' => $speciality->getSpecialityName(),
        ]);
        return true;
    }

}
