<?php

class DBConnect {
    private $connexion;

    public function __construct()
    {
        try {
            $db = [
                'host' => 'localhost', 
                'db' => 'kgb', 
                'port' => 3306,
                'charset' => 'utf8',
                'user' => 'admin',
                'password' => 'admin'
            ];
            $this->connexion = new \PDO("mysql:host=".$db['host'].";dbname=".$db['db'].";port=".$db['port'].";charset=".$db['charset']."", $db['user'], $db['password']);
        } catch (\PDOException $e) {
            echo 'Échec lors de la connexion : ' . $e->getMessage();
        }
    }

    public function getConnexion()
    {
        return $this->connexion;
    }
}