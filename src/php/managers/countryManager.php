<?php

include_once dirname(__DIR__).'/models/country.php';

class CountryManager extends DBConnect {

    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM country');

        while($row = $stmt->fetch()) {
            $country = new Country();
            $country->setId($row['country_id']);
            $country->setName($row['country_name']);
            $country->setNationality($row['country_nationality']);

            $result[] = $country;
        }

        return $result;
    }

    
    public function addCountry($country)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO country (country_name, country_nationality) VALUES (:pName, :pNationality)');
        $stmt->execute([
            'pName' => $country->getName(),
            'pNationality' => $country->getNationality()
        ]);
        return $country;
    }
}   