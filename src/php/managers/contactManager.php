<?php

include dirname(__DIR__).'/models/contact.php';

class ContactManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM contact');

        while($row = $stmt->fetch()) {
            $contact = new Contact();
            $contact->setId($row['contact_id']);
            $contact->setCode($row['contact_code']);
            $contact->setLastName($row['contact_last_name']);
            $contact->setFirstName($row['contact_first_name']);
            $contact->setBirth($row['contact_birth']);
            $contact->setNationality($row['contact_nationality']);

            $result[] = $contact;
        }

        return $result;
    }

    public function addContact($contact)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO contact (contact_code, contact_last_name, contact_first_name, contact_birth, contact_nationality) VALUES (:cCode, :cLastName, :cFirstName, :cBirth, :cNationality);');
        $stmt->execute([
            'cCode' => $contact->getCode(),
            'cLastName' => $contact->getLastName(),
            'cFirstName' => $contact->getFirstName(),
            'cBirth' => $contact->getBirth(),
            'cNationality' => $contact->getNationality(),
        ]);
        return true;
    }
}