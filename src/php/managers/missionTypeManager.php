<?php

include_once dirname(__DIR__).'/models/mission_type.php';

class MissionTypeManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM mission_type');

        while($row = $stmt->fetch()) {
            $missionType = new MissionType();

            $missionType->setId($row['mission_type_id']);
            $missionType->setMissionType($row['mission_type_name']);

            $result[] = $missionType;
        }

        return $result;
    }    

    public function addMissionType($missionType)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO mission_type (mission_type_name) VALUES (:mtName)');
        $stmt->execute([
            'mtName' => $missionType->getMissionType()
        ]);
    }
}
