<?php

include_once dirname(__DIR__).'/models/status.php';

class StatusManager extends DBConnect {


    /**
     * @return array
     */
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM status_type');


        while($row = $stmt->fetch()) {
            $status = new Status();
            $status->setId($row['status_id']);
            $status->setStatusName($row['status_name']);


            $result[] = $status;
        }

        return $result;
    }


    /**
     * @param $status
     * @return mixed
     */
    public function addStatus($status)
    {
        $stmt = $this->getConnexion()->prepare('INSERT INTO status_type (status_name) VALUES (:stName)');
        $stmt->execute([
            'stName' => $status->getStatusName(),
        ]);
        return $status;
    }
}   