<?php

include_once dirname(__DIR__).'/models/mission.php';

class MissionManager extends DBConnect {
    public function getAll(): array
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM mission');

        while($row = $stmt->fetch()) {
            $mission = new Mission();

            $mission->setId($row['mission_code']);
            $mission->setTitle($row['mission_title']);
            $mission->setDescription($row['mission_description']);
            $mission->setCountry($row['mission_country']);
            $mission->setAgent($row['mission_agent']);
            $mission->setContact($row['mission_contact']);
            $mission->setTarget($row['mission_target']);
            $mission->setType($row['mission_type']);
            $mission->setStatus($row['mission_status']);

            $mission->setHideout($row['mission_nb_hideout']);
            $mission->setSpeciality($row['mission_speciality']);

            $result[] = $mission;
        }

        return $result;
    }


}
