
<?php

    require_once './src/php/managers/bd_connect.php';
    require_once './src/php/models/person.php'

?>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Une Mission</h4>

    <form action="./src/php/tools/exe_contact.php" method="post">
        <div>
            <label for="missionTitle">Titre De La Mission (*)</label>
            <input type="text" placeholder="Titre De La Mission" name="missionTitle" id="missionTitle" required>
        </div>

        <div>
            <label for="missionDescription">Description De La Mission (*)</label>
            <input type="text" placeholder="Description De La Mission" name="missionDescription" id="missionDescription" required>
        </div>

        <div>
            <label for="missionDescription">Description De La Mission (*)</label>
            <input type="text" placeholder="Description De La Mission" name="missionDescription" id="missionDescription" required>
        </div>

        <div>
            <label for="missionCountry">Pays De La Mission (*)</label>
            <select name="missionCountry" id="missionCountry" >
                <?php require './src/php/utils/selects/country.php' ?>
            </select>
        </div>

        <div>
            <label for="missionAgent">Agent Requis Pour Cette Mission (*)</label>
            <select name="missionCountry" id="missionCountry" >
                <?php require './src/php/utils/selects/agent.php' ?>
            </select>
        </div>

        <div>
            <label for="missionContact">Contact De La Mission (*)</label>
            <select name="missionContact" id="missionContact" >
                <?php require './src/php/utils/selects/contact.php' ?>
            </select>
        </div>

        <div>
            <label for="missionContact">Cible À Tuer (*)</label>
            <select name="missionContact" id="missionContact" >
                <?php require './src/php/utils/selects/target.php' ?>
            </select>
        </div>

        <div>
            <label for="missionContact">Type De Mission (*)</label>
            <select name="missionContact" id="missionContact" >
                <?php require './src/php/utils/selects/missionType.php' ?>
            </select>
        </div>

        <div>
            <label for="missionContact">Status De La Mission (*)</label>
            <select name="missionContact" id="missionContact" >
                <?php require './src/php/utils/selects/missionStatus.php' ?>
            </select>
        </div>

        <div>
            <label for="missionContact">Planque Pour La Mission (*)</label>
            <select name="missionContact" id="missionContact" >
                <?php require './src/php/utils/selects/missionStatus.php' ?>
            </select>
        </div>

        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Un Contact</h4>

    <form action="./src/php/tools/exe_contact.php" method="post">
        <div>
            <label for="contactCode">Code Du Contact (*)</label>
            <input type="text" placeholder="Code Du Contact" name="contactCode" id="contactCode" required>
        </div>
        <div>
            <label for="contactLastName">Nom Du Contact (*)</label>
            <input type="text" placeholder="Nom Du Contact" name="contactLastName" id="contactLastName" required>
        </div>
        <div>
            <label for="contactFirstName">Prenom Du Contact (*)</label>
            <input type="text" placeholder="Prenom Du Contact" name="contactFirstName" id="contactFirstName" required>
        </div>
        <div>
            <label for="contactBirth">Date De Naissance Du Contacte</label>
            <input type="date" placeholder="Date De Naissance De L'Agent" name="contactBirth" id="contactBirth">
        </div>

        <div>
            <label for="agentNationality">Nationalité de l'agent (*)</label>
            <select name="agentNationality" id="agentNationality" >

                <?php require './src/php/utils/selects/nationality.php' ?>

            </select>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Un Agent</h4>

    <form action="./src/php/tools/exe_agent.php" method="post">
        <div>
            <label for="agentCode">Code de l'agent (*)</label>
            <input type="text" placeholder="Nom de code de l'argent" name="agentCode" id="agentCode" required>
        </div>
        <div>
            <label for="agentLastName">Nom de l'Agent (*)</label>
            <input type="text" placeholder="Nom de l'Agent" name="agentLastName" id="agentLastName" required>
        </div>
        <div>
            <label for="agentFirstName">Prenom de L'Agent (*)</label>
            <input type="text" placeholder="Prenom de L'Agent" name="agentFirstName" id="agentFirstName" required>
        </div>
        <div>
            <label for="agentBirth">Date De Naissance De L'Agent</label>
            <input type="date" placeholder="Date De Naissance De L'Agent" name="agentBirth" id="agentBirth" required>
        </div>
        
        <div>
            <label for="agentNationality">Nationalité de l'agent (*)</label>
            <select name="agentNationality" id="agentNationality" >

                <?php require './src/php/utils/selects/nationality.php' ?>
                
            </select>
        </div>

        <div>
            <label for="agentSpeciality">Specialité de l'Agent (*)</label>
            <select name="agentSpeciality" id="agentSpeciality">

                <?php require './src/php/utils/selects/speciality.php' ?>

            </select>
        </div>

        <button type="submit">Envoyez</button>
    </form>
</div>


<div style="margin-bottom: 3rem;">
    <h4>Ajouter Une Cible</h4>

    <form action="./src/php/tools/exe_target.php" method="post">
        <div>
            <label for="targetCode">Code de la cible (*)</label>
            <input type="text" placeholder="Code de l'agent" name="targetCode" id="targetCode" required>
        </div>
        <div>
            <label for="targetLastName">Nom de la Cible (*)</label>
            <input type="text" placeholder="Nom de l'Agent" name="targetLastName" id="targetLastName" required>
        </div>
        <div>
            <label for="targetFirstName">Prenom de La Cible (*)</label>
            <input type="text" placeholder="Prenom de L'Agent" name="targetFirstName" id="targetFirstName" required>
        </div>
        <div>
            <label for="targetBirth">Date De Naissance De La Cible</label>
            <input type="date" placeholder="Date De Naissance De L'Agent" name="targetBirth" id="targetBirth" required>
        </div>
        
        <div>
            <label for="targetNationality">Nationalité de la Cible (*)</label>
            <select name="targetNationality" id="targetNationality">

                <?php require './src/php/utils/selects/nationality.php' ?>
                
            </select>
        </div>


        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Un Type De Mission</h4>

    <form action="./src/php/tools/exe_mission_type.php" method="post">
        <div>
            <label for="missionType">Type de mission (*)</label>
            <input type="text" placeholder="Type de mission" name="missionType" id="missionType" required>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Une Planque</h4>

    <form action="./src/php/tools/exe_hideout.php" method="post">
        <div>
            <label for="hideoutCode">Nom De Code De La Planque (*)</label>
            <input type="text" placeholder="Code de la planque" name="hideoutCode" id="hideoutCode" required>
        </div>
        <div>
            <label for="hideoutCountry">Pays De La Planque (*)</label>
            <select name="hideoutCountry" id="hideoutCountry">

                <?php require './src/php/utils/selects/country.php' ?>
                
            </select>
        </div>
        <div>
            <label for="hideoutAddress">Adresse De La Plaque (*)</label>
            <input type="text" placeholder="Type de planque" name="hideoutAddress" id="hideoutAddress" required>
        </div>
        <div>
            <label for="hideoutType">Type De Planque (*)</label>
            <select name="hideoutType" id="hideoutType">

                <?php require './src/php/utils/selects/hideout_type.php' ?>

            </select>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Un Type De Planque</h4>

    <form action="./src/php/tools/exe_hideout_type.php" method="post">
        <div>
            <label for="hideoutType">Type de planque (*)</label>
            <input type="text" placeholder="Type de planque" name="hideoutType" id="hideoutType" required>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Une Spécialité</h4>

    <form action="./src/php/tools/exe_speciality.php" method="post">
        <div>
            <label for="specialityName">Nom de la Spécialité (*)</label>
            <input type="text" placeholder="Nom de la Spécialité" name="specialityName" id="specialityName" required>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Un Status</h4>

    <form action="./src/php/tools/exe_status.php" method="post">
        <div>
            <label for="statusName">Nom du Status (*)</label>
            <input type="text" placeholder="Status de Mission" name="statusName" id="statusName" required>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div>

<div style="margin-bottom: 3rem;">
    <h4>Ajouter Un Admin</h4>

    <form action="./src/php/tools/exe_admin.php" method="post">
        <div>
            <label for="pseudo">Pseudo (*)</label>
            <input type="text" placeholder="Pseudo" name="pseudo" id="pseudo" required>
        </div>
        <div>
            <label for="lastName">Nom (*)</label>
            <input type="text" placeholder="Nom de famille" name="lastName" id="lastName" required>
        </div>
        <div>
            <label for="firstName">Prenom (*)</label>
            <input type="text" placeholder="Prenom" name="firstName" id="firstName" required>
        </div>
        <div>
            <label for="addressMail">Adresse e-mail (*)</label>
            <input type="email" placeholder="Adresse e-mail" name="addressMail" id="addressMail" required>
        </div>
        <div>
            <label for="password">Mots de passe (*)</label>
            <input type="password" placeholder="Mots de passe" name="password" id="password" required>
        </div>
        
        <button type="submit">Envoyez</button>
    </form>
</div>

<div>
    <h4>Ajouter Un Pays</h4>
    <form action="./src/php/tools/exe_country.php" method="post">
        <div>
            <label for="countryName">Nom du Pays (*)</label>
            <input type="text" placeholder="Nom du pays" name="countryName" id="countryName" required>
        </div>
        <div>
            <label for="countryNationality">Nationalité du Pays (*)</label>
            <input type="text" placeholder="Nationalité" name="countryNationality" id="countryNationality" required>
        </div>
        <button type="submit">Envoyez</button>
    </form>
</div> 