<header>
    <nav class="navbar navbar-expand-md kgb-bg-light rounded-bottom shadow-sm">
        <div>
             <a href="">
                <img class="navbar-brand logo" src="./src/img/logo_hero.png" alt="Logo du KGB">
            </a>
            <h5 class="d-inline-block align-bottom text-dark pb-3">Comité de sécurité KBG</h5>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav d-flex w-100 justify-content-end text-uppercase ">
                <!-- <li class="nav-item">
                    <a class="nav-link btn btn-kgb m-1 border border-primary rounded" href="index.php">Accueil</a>
                </li> -->
                <?php if(!isset($_SESSION['connected'])){ ?>
                    <li class="nav-item ">
                        <a class="btn-kgb nav-link btn m-1 rounded" id="connexion" href="src/php/utils/connexion.php">Se Connecter</a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item ">
                        <a class="btn-kgb nav-link btn m-1 rounded" id="connexion" href="src/php/utils/connexion.php">Se Déconnecter</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>
