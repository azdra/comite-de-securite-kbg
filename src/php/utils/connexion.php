<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/index.css">
</head>
<body>
    <div class="body-container">
        <div class="">
            <!-- include_once HEADER -->
                <?php include_once './header.php' ?>
            <!-- include_once HEADER -->

            <main class="container d-flex justify-content-center  ">
                <form class="kgb-bg-light mt-4 pb-3 px-4 rounded text-center form-container" action="./src/php/models/target.php" method="POST" enctype="multipart/form-data">
                    <div>
                        <label for="email" class="white mt-5">Email:</label>
                        <input class="form-control" type="email" name="email" id="email">
                        <label for="mdp" class="white mt-2">Mot de passe:</label>
                        <input class="form-control" type="password" name="mdp" id="mdp">
                        <button class="btn btn-kgb text-center bg-light mt-3" type="submit">Connexion</button>
                    </div>
                </form>
            </main>
        </div>
    </div>
</body>
</html>
