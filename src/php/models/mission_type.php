<?php

class MissionType {
    private int $id;
    private string $missionType;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMissionType(): string
    {
        return $this->missionType;
    }

    /**
     * @param string $missionType
     */
    public function setMissionType(string $missionType): void
    {
        $this->missionType = $missionType;
    }

}