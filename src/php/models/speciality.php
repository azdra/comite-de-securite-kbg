<?php

class Speciality {
    private int $id;
    private string $specialityName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSpecialityName(): string
    {
        return $this->specialityName;
    }

    /**
     * @param string $specialityName
     */
    public function setSpecialityName(string $specialityName): void
    {
        $this->specialityName = $specialityName;
    }
}