<?php

class Mission {
    private int $id;
    private string $title;
    private string $description;
    private int $country;
    private int $agent;
    private int $contact;
    private int $target;
    private int $type;
    private int $status;
    private int $hideout;
    private int $speciality;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getCountry(): int
    {
        return $this->country;
    }

    /**
     * @param int $country
     */
    public function setCountry(int $country): void
    {
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getAgent(): int
    {
        return $this->agent;
    }

    /**
     * @param int $agent
     */
    public function setAgent(int $agent): void
    {
        $this->agent = $agent;
    }

    /**
     * @return int
     */
    public function getContact(): int
    {
        return $this->contact;
    }

    /**
     * @param int $contact
     */
    public function setContact(int $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return int
     */
    public function getTarget(): int
    {
        return $this->target;
    }

    /**
     * @param int $target
     */
    public function setTarget(int $target): void
    {
        $this->target = $target;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getHideout(): int
    {
        return $this->hideout;
    }

    /**
     * @param int $hideout
     */
    public function setHideout(int $hideout): void
    {
        $this->hideout = $hideout;
    }

    /**
     * @return int
     */
    public function getSpeciality(): int
    {
        return $this->speciality;
    }

    /**
     * @param int $speciality
     */
    public function setSpeciality(int $speciality): void
    {
        $this->speciality = $speciality;
    }
}