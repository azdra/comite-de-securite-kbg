<?php

class HideoutType {
    private int $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHideoutTypeName(): string
    {
        return $this->hideoutTypeName;
    }

    /**
     * @param string $hideoutTypeName
     */
    public function setHideoutTypeName(string $hideoutTypeName): void
    {
        $this->hideoutTypeName = $hideoutTypeName;
    }
    private string $hideoutTypeName;
}
