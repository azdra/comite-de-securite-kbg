<?php

class Agent extends Humans {
    private int $speciality;

    /**
     * @return int
     */
    public function getSpeciality(): int
    {
        return $this->speciality;
    }

    /**
     * @param int $speciality
     */
    public function setSpeciality(int $speciality): void
    {
        $this->speciality = $speciality;
    }


}