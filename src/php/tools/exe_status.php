<?php

require dirname(__DIR__) . "/managers/bd_connect.php";
require_once dirname(__DIR__)."/managers/statusManager.php";

$statusManager = new StatusManager();

$status = new Status();
$status->setStatusName( ucfirst(strtolower($_POST['statusName'])) );

$statusManager->addStatus($status);
sleep(2);
header('Location: ../../../index.php', true, 302);
