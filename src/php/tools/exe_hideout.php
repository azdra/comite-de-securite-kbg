<?php

require dirname(__DIR__) . "/managers/bd_connect.php";
require dirname(__DIR__) . "/managers/hideoutManager.php";

$hideoutManager = new HideoutManager();

$hideout = new Hideout();
$hideout->setCode($_POST['hideoutCode']);
$hideout->setCountry($_POST['hideoutCountry']);
$hideout->setAddress($_POST['hideoutAddress']);
$hideout->setType($_POST['hideoutType']);

$hideoutManager->addHideout($hideout);

sleep(2);
header('Location: ../../../index.php', true, 302);