<?php

require dirname(__DIR__) . "/managers/bd_connect.php";
require dirname(__DIR__) . "/managers/adminManager.php";

$adminManager = new AdminManager();
$admin = new Admin();

$password = password_hash($_POST['password'],PASSWORD_BCRYPT);

$admin->setPseudo($_POST['pseudo']);
$admin->setLastName($_POST['lastName']);
$admin->setFirstName($_POST['firstName']);
$admin->setMail($_POST['addressMail']);
$admin->setPassword($password);
$admin->setCreation(date('Y-m-d'));

$adminManager->addAdmin($admin);

sleep(2);
header('Location: ../../../index.php', true, 302);