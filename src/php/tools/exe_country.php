<?php

require dirname(__DIR__) . "/managers/bd_connect.php";
require dirname(__DIR__) . "/managers/countryManager.php";

$countryManager = new CountryManager();
$country = new Country();

$country->setName( ucfirst(strtolower($_POST['countryName'])) );
$country->setNationality( ucfirst(strtolower($_POST['countryNationality'])) );

$countryManager->addCountry($country);

sleep(2);
header('Location: ../../../index.php', true, 302);