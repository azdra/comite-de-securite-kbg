<?php

require dirname(__DIR__) . "/managers/bd_connect.php";
require_once dirname(__DIR__)."/managers/targetManager.php";

$targetManager = new TargetManager();

$target = new Target();
$target->setCode($_POST['targetCode']);
$target->setLastName($_POST['targetLastName']);
$target->setFirstName($_POST['targetFirstName']);
$target->setBirth($_POST['targetBirth']);
$target->setNationality($_POST['targetNationality']);

$targetManager->addTarget($target);

sleep(2);
header('Location: ../../../index.php', true, 302);
