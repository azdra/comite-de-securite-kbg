<?php

include dirname(__DIR__)."/managers/bd_connect.php";
include dirname(__DIR__)."/managers/agentManager.php";

$agentManager = new AgentManager();

$agent = new Agent();
$agent->setCode($_POST['agentCode']);
$agent->setLastName($_POST['agentLastName']);
$agent->setFirstName($_POST['agentFirstName']);
$agent->setBirth($_POST['agentBirth']);
$agent->setNationality($_POST['agentNationality']);
$agent->setSpeciality($_POST['agentSpeciality']);

$agentManager->addAgent($agent);

sleep(2);
header('Location: ../../../index.php', true, 302);