<?php

require dirname(__DIR__) . "/managers/bd_connect.php";
require_once dirname(__DIR__)."/managers/specialityManager.php";
$specialityManager = new SpecialityManager();

$speciality = new Speciality();
$speciality->setSpecialityName( ucfirst(strtolower($_POST['specialityName'])) );

$specialityManager->addSpeciality($speciality);
sleep(2);
header('Location: ../../../index.php', true, 302);